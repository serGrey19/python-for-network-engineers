"""
NAT = "ip nat inside source list ACL interface FastEthernet0/1 overload"   #Задание 4.1

NAT = NAT.replace('Fast', 'Gigabit')

print(NAT)
"""

"""
mac = 'AAAA:BBBB:CCCC'   #Задание 4.2

mac = mac.replace(':', '.')
mac1 = '.DDDD.EEEE.FFFF'
mac = mac + mac1

print(mac)
"""

"""
config = 'switchport trunk allowed vlan 1,3,10,20,30,100'   #Задание 4.3

configList = config.split()
configList = configList[-1].split(',')

print(configList)
"""

"""
vlans = [10, 20, 30, 1, 2, 100, 10, 30, 3, 4, 10]   #Задание 4.4

vlans = set(vlans)
vlans = sorted(vlans)

print(vlans)
"""

"""
command1 = 'switchport trunk allowed vlan 1,2,3,5,8'   #Задание 4.5
command2 = 'switchport trunk allowed vlan 1,3,8,9'

command1 = command1.split()
command1 = command1[-1].split(',')
command1 = set(command1)

command2 = command2.split()
command2 = command2[-1].split(',')
command2 = set(command2)

unionCommand = command1 & command2
unionCommand = sorted(unionCommand)

print(unionCommand)
"""


ospf_route = 'O        10.0.24.0/24 [110/41] via 10.0.13.3, 3d18h, FastEthernet0/0'   #Задание 4.6

ospf_route = ospf_route.split()
protocol = "OSPF"
prefix = ospf_route[1]
ad_metric = ospf_route[2]
ad_metric = ad_metric.sttrip('[]')
next_hop = ospf_route[4]
last_update = ospf_route[5]
outbound_interface = ospf_route[6]

Protocol = "Protocol"
Prefix = "Prefix"
AD_Metric = "AD/Metric"
Next_Hop = "Next-Hop"
Last_update = "Last update"
Outbound_Interface = "Outbound Interface"

print(f'''
    {Protocol:<24} {protocol:<24} 
    {Prefix:<24} {prefix:<24} 
    {AD_Metric:<24} {ad_metric:<24}
    {Next_Hop:<24} {next_hop:<24}
    {Last_update:<24} {last_update:<24}
    {Outbound_Interface:<24} {outbound_interface:<24}''')


"""
mac = 'AAAA:BBBB:CCCC'   #Задание 4.7

mac = mac.split(':')
mac = ''.join(mac)
mac = int(mac, 16)
mac = bin(mac)

print(mac)
"""

"""
ip = '192.168.3.1'   #Задание 4.8
ip = ip.split('.')
fi = ip[0]
fi = int(fi)
fi2 = bin(fi)
s = ip[1]
s = int(s)
s2 = bin(s)
t = ip[2]
t = int(t)
t2 = bin(t)
fo = ip[3]
fo = int(fo)
fo2 = bin(fo)


ip_template = '''
    IP address:
    {0:<10} {1:<10} {2:<10} {3:<10}
    {0:<10} {1:<10} {2:<10} {3:<10}
    '''

print(ip_template.format(fi, s, t, fo))
"""