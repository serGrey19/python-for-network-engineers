"""
mac = ['aabb:cc80:7000', 'aabb:dd80:7340', 'aabb:ee80:7000', 'aabb:ff80:7000']    #Задание 6.1

mac_cisco = []

for mac in mac:
    mac_cisco.append(mac.replace(':', '.'))

print(mac_cisco)
"""


"""                                                                            #Задание 6.2b
right = False
n = 1
while right == False:
    try:
        ip = input('Введите ip-адрес: ')
        ip_split = ip.split('.')
        for num in ip_split:
            num = int(num)
            if (num > 0) and (num < 256):           
                pass
            else:
                n = 0
                break
        if n == 0:
            print('Неправильно введён ip-адрес')
            break
        else:
            if len(ip_split) != 4 :
                print('Неправильно введён ip-адрес')
            else:
                right = True
    except ValueError:
        print('Неправильно введён ip-адрес')

if right == True:
    ip_split = ip.split('.')
    ip_last = ip_split[3]
    ip_last = int(ip_last)

    if ip == '255.255.255.255':
        print("local broadcast")
    elif ip == '0.0.0.0':
        print("unassigned")
    elif ip_last > 0 and ip_last < 224:
        print("unicast")
    elif ip_last > 223 and ip_last < 240:
        print("multicast")
    else:
        print("unused")
"""

"""
access_template = [                                                                  #Задание 6.3
    'switchport mode access', 'switchport access vlan',
    'spanning-tree portfast', 'spanning-tree bpduguard enable'
]

trunk_template = [
    'switchport trunk encapsulation dot1q', 'switchport mode trunk',
    'switchport trunk allowed vlan'
]

access = {
    '0/12': '10',
    '0/14': '11',
    '0/16': '17',
    '0/17': '150'
}
trunk = {
        '0/1': ['add', '10', '20'],
        '0/2': ['only', '11', '30'],
        '0/4': ['del', '17']
    }

for intf, vlan in access.items():
    print('interface FastEthernet' + intf)
    for command in access_template:
        if command.endswith('access vlan'):
            print(' {} {}'.format(command, vlan))
        else:
            print(' {}'.format(command))

print('-'*40)
i = 3
while i != 0:
    for intf, vlan in trunk.items():
        if  trunk[intf][0] == 'add':
            add = trunk[intf][1::]
            add = ','.join(add)
            i -= 1
        elif trunk[intf][0] == 'only':
            only = trunk[intf][1::]
            only = ','.join(only)
            i -= 1
        elif trunk[intf][0] == 'del':
            dele = trunk[intf][1::]
            dele = ','.join(dele)
            i -= 1

for intf, vlan in trunk.items():
    print('interface FastEthernet' + intf)
    for command in trunk_template:
        if command.endswith('allowed vlan'):
            if trunk[intf][0] == 'add':
                print(' {} add {}'.format(command,add))
            elif trunk[intf][0] == 'only':
                print(' {} {}'.format(command,only))
            elif trunk[intf][0] == 'del':
                print(' {} remove {}'.format(command,dele))
        else:
            print(' {}'.format(command))
"""