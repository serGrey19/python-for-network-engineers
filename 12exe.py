"""
import subprocess                                                                  #Задание 12.1

def ping_ip_addresses(ips):
    ipst = []
    ipsf = []
    for ip in ips:
        reply = subprocess.run(['ping', '-c', '3', '-n', ip])
        if reply.returncode == 0:
            ipst.append(ip)
        else:
            ipsf.append(ip)
    return ipst,ipsf

ip_list = ('1111.1111.1111.1111', '74.125.143.113')
final = ping_ip_addresses(ip_list)
print(final)
"""


import subprocess                                                                  #Задание 12.2 12.3
import ipaddress
from tabulate import tabulate

def check_if_ip_is_network(ip_address):
    try:
        ipaddress.ip_network(ip_address)
        return True
    except ValueError:
        return False

def ping_ip_addresses(ips):
    ipst = []
    ipsf = []
    for ip in ips:
        if ip.find('-') != -1:
            ipsp = ip.split('-')
            ipyon = check_if_ip_is_network(ipsp[1])
            if ipyon == True:
                ip0 = ipaddress.ip_address(ipsp[0])
                ip1 = ipaddress.ip_address(ipsp[1])
                while ip0 <= ip1:
                    reply = subprocess.run(['ping', '-c', '3', '-n', str(ip0)])
                    if reply.returncode == 0:
                        ipst.append(str(ip0))
                    else:
                        ipsf.append(str(ip0))
                    ip0 += 1
            else:
                ip0 = ipaddress.ip_address(ipsp[0])
                ip_last = int((ipsp[0].split('.'))[3])
                ip1 = int(ipsp[1])
                ip1 = ip1 - ip_last + 1
                while ip1 != 0:
                    reply = subprocess.run(['ping', '-c', '3', '-n', str(ip0)])
                    if reply.returncode == 0:
                        ipst.append(str(ip0))
                    else:
                        ipsf.append(str(ip0))
                    ip1 -= 1
                    ip0 += 1
        else:
            reply = subprocess.run(['ping', '-c', '3', '-n', ip])
            if reply.returncode == 0:
                ipst.append(ip)
            else:
                ipsf.append(ip)
    return ipst,ipsf

ip_list = ('255.255.255.255', '74.125.143.113', '74.125.143.113-74.125.143.115', '74.125.143.111-113')
final = ping_ip_addresses(ip_list)
print(final)

def print_ip_table(reachable, unreachable):
    head = ('Reachable', 'Unreachable')
    re_len = len(reachable)
    un_len = len(unreachable)
    if re_len > un_len:
        un_len = re_len - un_len
        while un_len != 0:
            unreachable.append('')
            un_len -= 1
    elif re_len < un_len:
        re_len = un_len - re_len
        while re_len != 0:
            reachable.append('')
            re_len -= 1
    data = zip(reachable, unreachable)
    print(tabulate(data, headers = head))

print_ip_table(final[0], final[1])
