"""
london_co = {                                                                       #Задание 5.1d
    'r1': {
        'location': '21 New Globe Walk',
        'vendor': 'Cisco',
        'model': '4451',
        'ios': '15.4',
        'ip': '10.255.0.1'
    },
    'r2': {
        'location': '21 New Globe Walk',
        'vendor': 'Cisco',
        'model': '4451',
        'ios': '15.4',
        'ip': '10.255.0.2'
    },
    'sw1': {
        'location': '21 New Globe Walk',
        'vendor': 'Cisco',
        'model': '3850',
        'ios': '3.6.XE',
        'ip': '10.255.0.101',
        'vlans': '10,20,30',
        'routing': True
    }
}

name_d = input('Введите имя устройства: ')
dic_name = london_co[name_d]
print(dic_name)
keys = list(dic_name.keys())
keys = ','.join(keys)
print(keys)
name_s = input('Введите имя параметра ({}): '.format(keys))
name_s = name_s.lower()
print(dic_name.get(name_s, 'Такого параметра нет'))
"""

"""
ip = input('Введите адрес IP-сети в формате 10.1.1.0/24: ')                                    #Задание 5.2b
#from sys import ip

ip = ip.split('/')
ipa = ip[0]
ipm = ip[1]
ipm = int(ipm)
ipa = ipa.split('.')

ipa0 = ipa[0]
ipa1 = ipa[1]
ipa2 = ipa[2]
ipa3 = ipa[3]
ipa0 = int(ipa[0])
ipa1 = int(ipa[1])
ipa2 = int(ipa[2])
ipa3 = int(ipa[3])

if ipa3 == 0:
    ipa3 = 1

ipm0 = 32 - ipm
ipm01 ='1'*ipm + '0'*ipm0
print(ipm01)
ipm1 = ipm01[0:8]
print(ipm1)
ipm2 = ipm01[8:16]
ipm3 = ipm01[16:24]
ipm4 = ipm01[24:32]
ipma1 = int(ipm1, 2)
ipma2 = int(ipm2, 2)
ipma3 = int(ipm3, 2)
ipma4 = int(ipm4, 2)

ip_final = '''
    Network:
    {0:<8} {1:<8} {2:<8} {3:<8}
    {0:08b} {1:08b} {2:08b} {3:08b}

    Mask:
    /{4:<8}
    {9:<8} {10:<8} {11:<8} {12:<8}
    {5:<8} {6:<8} {7:<8} {8:<8}
    '''
print(ip_final.format(ipa0, ipa1, ipa2, ipa3, ipm, ipm1, ipm2, ipm3, ipm4, ipma1, ipma2, ipma3, ipma4))
"""

"""
access_template = [                                                               #Задание 5.3а
    'switchport mode access', 'switchport access vlan {}',
    'switchport nonegotiate', 'spanning-tree portfast',
    'spanning-tree bpduguard enable'
]

trunk_template = [
    'switchport trunk encapsulation dot1q', 'switchport mode trunk',
    'switchport trunk allowed vlan {}'
]

mode_dict = {
    "access": [access_template, "Введите номер VLAN: "],
    "trunk": [trunk_template, "Введите разрешенные VLANы: "]
}
mode = input('Введите режим работы интерфейса (access/trunk): ')
inter = input('Введите тип и номер интерфейса: ')
vlan = input(mode_dict[mode][1])
print('\ninterface {}'.format(inter))
print('\n'.join(mode_dict[mode][0]).format(vlan))
"""