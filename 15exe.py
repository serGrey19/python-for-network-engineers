"""
import re                                                                                     #Задание 15.1b
def get_ip_from_cfg(filename):
    final = {}
    ip = ()
    ethernet =''
    e = ''
    ip_list = []
    with open(filename, 'r') as f:                                                                 
        for line in f:
            ethernet = re.search(r'interface (\S+)', line)
            ip = re.search(r' ip address (\S+) (\S+)', line)
            if ethernet:
                e = ethernet.group(1)
            elif ip and line.startswith(' '):
                if e in final:
                    final[e].append(ip.groups())
                else:
                    ip_list.append(ip.groups())
                    final.update({e:ip_list})
                    ip_list = []
    return final
final = get_ip_from_cfg('config_r2.txt')
print(final)
"""

"""
import re                                                                                     #Задание 15.2a
def parse_sh_ip_int_br(filename):
    with open(filename, 'r') as f: 
        final = []       
        lisT = ()        
        i = 0                                                 
        for line in f:
            showip = re.search(r'(\S+)\s+([\w.]+)[ \w]*(up|administratively down)\s+(\S+)',line)
            if showip:
                final.append(showip.groups())
        for s in final:
            if s[2] == 'administratively down':
                lisT = list(s)
                lisT[2] = 'down'
                final[i] = tuple(lisT)
            i += 1
        i = 0
    return final
final = parse_sh_ip_int_br('sh_ip_int_br.txt')
print(final)

headers = ['Interface', 'IP-Address', 'Status', 'Protocol']

def convert_to_dict(head, parse):
    final_list = []
    di = {}
    for li in parse:
        for i, item in enumerate(li):
            di[headers[i]] = item
        final_list.append(di)
        di = {}
    return final_list
final_list = convert_to_dict(headers, final)
print(final_list)
"""

"""
import re                                                                                        #Задание 15.3
def convert_ios_nat_to_asa(file_in, file_out):
    with open(file_in, 'r') as f: 
        for line in f:
            ip_port = re.search(r'\d+.\d+.\d+.\d+ (\d+)',line).group()
            nat = re.search(r'\d+$',line).group()
            ip_port = ip_port.split()
            ip = ip_port[0]
            port = ip_port[1]
            line = 'object network LOCAL_'+ip+'\n host '+port+'\n nat (inside,outside) static interface service tcp '+nat+'\n'
            with open(file_out, 'a') as f:
                f.write(line)        
convert_ios_nat_to_asa('cisco_nat_config.txt', 'cisco_asa_config.txt')
"""

"""
import re                                                                                        #Задание 15.4
def convert_ios_nat_to_asa(file):
    with open(file, 'r') as f: 
        inter = None
        no_des = []
        for line in f:  
            if inter != None:
                des = line.find('description')
                if des == -1:
                    no_des.append(inter.group())
                    inter = None
                else:
                    inter = None
            else:
                inter = re.search(r'^interface \S+',line)
    return no_des
print(convert_ios_nat_to_asa('config_r1.txt'))
"""


import re                                                                                  #Задание 15.5
def generate_description_from_cdp(file):
    with open(file, 'r') as f: 
        final_dict = {}
        des = ''
        for i, line in enumerate(f):  
            if i >= 6:
                device_inter = re.search(r'^\S+\s+\S+ \S+',line).group()
                device_inter = device_inter.split()
                device = device_inter[0]
                inter = device_inter[1:]
                inter = ' '.join(inter)
                port = re.search(r'\S+ \S+$',line).group()
                des = 'description Connected to '+device+' port '+port
                final_dict[inter] = des
    return final_dict
print(generate_description_from_cdp('sh_cdp_n_sw1.txt'))
