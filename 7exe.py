"""
with open('F:/python/ospf.txt', 'r') as f:                                                                   #Задание 7.1
    for line in f:
        ospf_route = line.replace('O', 'OSPF').replace('[', '').replace(',', ' ').replace(']', '')
        ospf_route = ospf_route.split()
        ospf_route.remove('via')
        print(ospf_route)
        protocol, prefix, ad_metric, next_hop, last_update, outbound_interface = ospf_route

        print(f'''
            {'Protocol':<24} {protocol:<24} 
            {'Prefix':<24} {prefix:<24} 
            {'AD_Metric':<24} {ad_metric:<24}
            {'Next_Hop':<24} {next_hop:<24}
            {'Last_update':<24} {last_update:<24}
            {'Outbound_Interface':<24} {outbound_interface:<24}''')
"""

"""
from sys import argv                                                                              #Задание 7.2c
beg = argv[1]
end = argv[2]
ignore = ['duplex', 'alias', 'Current configuration']                                            
ig = 0
c = 0
count = len(ignore)
clear = ''
with open(beg, 'r') as f:                                                              
    for line in f:
        if line.startswith('!'):
            clear = clear + line 
        else:            
            for word in ignore:
                if (line.find(word) != -1) :
                  pass
                else:
                    ig += 1
                if ig == 3:
                    print(line.rstrip())
                    clear = clear + line 
                c += 1
                if c == count:
                    c = 0
                    ig = 0
print(clear)

f = open(end, 'w')
f.write(clear)
f.close()
"""

"""
lin = 0                                                                                        #Задание 7.3
mac_dict = {'test': 'test'}
with open('F:/python/CAM_table.txt', 'r') as f:                                                                   
    for line in f:
        if lin == 7:
            line = line.split()
            mac_dict[line[0]] = line[0::]
            

        else:
            lin +=1
del mac_dict['test']

for vlan, mac in mac_dict.items():
    print(f'''{mac[0]:<8} {mac[1]:<20} {mac[2]:<8} ''')

print('-'*40)

for key in sorted(mac_dict.keys()):
    print(f'''{mac_dict[key][0]:<8} {mac_dict[key][1]:<20} {mac_dict[key][2]:<8} ''')

print('-'*40)

vlan = input('Введите Vlan: ')
print(f'''{mac_dict[vlan][0]:<8} {mac_dict[vlan][1]:<20} {mac_dict[vlan][2]:<8} ''')
"""