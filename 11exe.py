"""
def parse_cdp_neighbors(command_output):                                                           #Задание 11.1
    final_dict = {}
    command_output = command_output.split('\n')
    key1 = command_output[0][0:command_output[0].find('>')]
    for pos, s in enumerate(command_output): 
        if s.startswith('Device ID'): 
            pos += 1
            break  
    command_output = command_output[pos::]
    for line in command_output:
        line = line.split()
        key2 = line[1:3]
        key2 = ''.join(key2)
        item = line[7:9]
        item = ''.join(item)
        final_dict[(key1, key2)] = (line[0], item)
    return final_dict
str_sh = ''
with open('sh_cdp_n_sw1.txt', 'r') as f:                                                                   
        for line in f:
            str_sh = str_sh + line

final_dict = parse_cdp_neighbors(str_sh)
print(final_dict)
"""


def create_network_map(filenames):                                        #Задание 11.2
    final_dict = {}
    
    for fil in filenames:
        file_str = ''
        with open(fil, 'r') as f:                                                                 
            for line in f:
                file_str = file_str + line
            file_str = file_str.split('\n')
            key1 = file_str[0][0:file_str[0].find('>')]           
            for pos, s in enumerate(file_str): 
                if s.startswith('Device ID'): 
                    pos += 1
                    break
            file_str = file_str[pos::]
            for line in file_str:
                nums = 7
                nume = 9
                line = line.split()
                if line[4] == 'R':
                    nums = 8
                    nume = 10
                key2 = line[1:3]
                key2 = ''.join(key2)
                keyf = (key1, key2)
                item = line[nums:nume]
                item = ''.join(item)                   
                if key1[0:2] == 'SW':
                    final_dict[keyf] = (line[0], item)
                else:
                    final_dict[(line[0], item)] = keyf
    return final_dict

fnames = ['sh_cdp_n_sw1.txt', 'sh_cdp_n_r1.txt', 'sh_cdp_n_r2.txt', 'sh_cdp_n_r3.txt']
final_dict = create_network_map(fnames)
print(final_dict)

from draw_network_graph import draw_topology
if __name__ == '__main__':
    draw_topology(final_dict)
