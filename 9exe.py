"""
access_mode_template = [                                                                             #Задание 9.1a
    'switchport mode access', 'switchport access vlan',
    'switchport nonegotiate', 'spanning-tree portfast',
    'spanning-tree bpduguard enable'
]

port_security_template = [
    'switchport port-security maximum 2',
    'switchport port-security violation restrict',
    'switchport port-security'
]

access_config = {
    'FastEthernet0/12': 10,
    'FastEthernet0/14': 11,
    'FastEthernet0/16': 17
}

def generate_access_config(intf_vlan_mapping, access_template, psecurity = None):
    
    intf_vlan_mapping - словарь с соответствием интерфейс-VLAN такого вида:
        {'FastEthernet0/12':10,
         'FastEthernet0/14':11,
         'FastEthernet0/16':17}
    access_template - список команд для порта в режиме access

    Возвращает список всех портов в режиме access с конфигурацией на основе шаблона
    
    final = []
    for intf, vlan in access_config.items():
        final.append('interface ' + intf)
        for command in access_template:
            if command.endswith('access vlan'):
                final.append(' {} {}'.format(command, vlan))
            else:
                final.append(' {}'.format(command))
        if psecurity == None:
            pass
        else:
            for k in psecurity:
                final.append(' {}'.format(k))
    return final

final = generate_access_config(access_config, access_mode_template, port_security_template)
print(final)
for k in final:
    print(k)
"""

"""
trunk_mode_template = [                                                                         #Задание 9.2а
    'switchport mode trunk', 'switchport trunk native vlan 999',
    'switchport trunk allowed vlan'
]

trunk_config = {
    'FastEthernet0/1': [10, 20, 30],
    'FastEthernet0/2': [11, 30],
    'FastEthernet0/4': [17]
}

def generate_trunk_config(intf_vlan_mapping, trunk_template):                       
    final = []
    dict_item = []
    allv =''
    key = ''
    final_dict = {}
    for intf, vlan in trunk_template.items():
        final.append('interface ' + intf)
        for command in intf_vlan_mapping:
            if command.endswith('allowed vlan'):
                for vl in vlan:
                    vl = str(vl)
                    allv = allv + str(vl) + ','
                allv = allv[::-1]
                allv = allv[1::]
                allv = allv[::-1]
                dict_item.append(' {} {}'.format(command, allv)) 
                final.append(' {} {}'.format(command, allv))
                allv =''
                key = 'interface ' + intf
                final_dict[key] = dict_item
                dict_item = []
            else:
                dict_item.append(' {} {}'.format(command, allv))
                final.append(' {}'.format(command))
    return final, final_dict

answer = generate_trunk_config(trunk_mode_template, trunk_config)

final = answer[0]
for k in final:
    print(k)

print('-'*40)

final_dict = answer[1]
print(final_dict)
"""

"""
def  get_int_vlan_map(config_filename):                                         #Задание 9.3а
    key = ''
    access = ''
    acc = 0
    commands = ''
    truck = []
    tr = 0
    access_dict = {}
    truck_dict = {}
    with open(config_filename, 'r') as f:                                                                   
        for line in f:
            if line.startswith('interface FastEthernet'):
                key = 'FastEthernet' + line[-4:]
                key = key.strip()
            if line.find('access vlan') != -1:
                access = line[24::]
                access = access.strip()
                acc = 1
            elif acc == 2:
                 acc = 1
            if line.find('trunk allowed') != -1:
                commands = line.split()
                truck = commands[-1].split(',')
                tr = 1
            if line.find('mode access') != -1:
                access = '1'
                acc = 2
            if (key != 0) and (acc == 1):
                access_dict[key] = access
                key = 0
                access = ''
                acc = 0
            if (key != 0) and (tr == 1):
                truck_dict[key] = truck
                key = 0
                truck = ''
                tr = 0
    return access_dict, truck_dict

final = get_int_vlan_map('config_sw2.txt')
print(final)
"""
        

ignore = ['duplex', 'alias', 'Current configuration']                                     #Задание 9.4
def convert_config_to_dict(config_filename):                                                     
    key = ''
    fkey = ''
    items = []
    final_dict = {}
    ignor = True
    with open(config_filename, 'r') as f:                                                                   
        for line in f:
            ignor = ignore_command(line, ignore)
            if line[0] != '!':
                if ignor == False:
                    if line[0] != ' ':
                        if key != 0:
                            fkey = key
                            key = line.strip()
                        else:
                            key = line.strip()
                    else:
                        items.append(line.strip())
            if (fkey != '') and (items !=[]):
                final_dict[fkey] = items
                fkey = ''
                items = []
            elif (fkey != ''):
                final_dict[fkey] = []
                fkey = ''
                items = []
        if (key != '') and (items !=[]):
            final_dict[key] = items
            key = ''
            items = []
        elif (key != ''):
            final_dict[key] = ''
            key = ''
            items = []       
    return final_dict

def ignore_command(command, ignore):                                              
    return any(word in command for word in ignore)

final = convert_config_to_dict('config_sw1.txt')
print(final)
